import React, { useEffect, useState } from "react";
import "./form.css";
import { ErrorMessage, Field, Formik, Form } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import {
  sortingAction,
  userFormAction,
  sortingCharacterAction,
  searchAction,
} from "../Redux/Actions/userFormAction";

const UserForm = () => {
  const [find, setFind] = useState("");
  const [check, setCheck] = useState([]);
  const dispatch = useDispatch();
  // To Show Table
  const [table, showTable] = useState(true);

  // For Sorting Age
  const [currentsort, setCurrentSort] = useState(true);
  const [config, setConfig] = useState(true);
  const searchinput = (e) => {
    setFind(e.target.value.toLowerCase());
    // find.toLowerCase();
    showTable(true);
  };
  const info = useSelector((state) => {
    return state?.detail?.data;
  });
  const searched = useSelector((state) => {
    return state?.detail?.search;
  });
  useEffect(() => {
    setCheck(searched);
  }, [searched]);
  // Search the Value
  const search = () => {
    showTable(false);
    dispatch(searchAction(find));
  };

  const onSortChange = () => {
    dispatch(sortingAction(currentsort));
    setCurrentSort(!currentsort);
  };
  // For Sorting A-z or z-A
  const requestSort = (key) => {
    dispatch(sortingCharacterAction(key, config));
    setConfig(!config);
  };

  // User Form
  return (
    <>
      <div className="dv">
        <h1>User Form</h1>

        <Formik
          initialValues={{
            Id: new Date().getUTCMilliseconds(),
            FirstName: "",
            LastName: "",
            Age: "",
            Email: "",
          }}
          validationSchema={Yup.object().shape({
            Email: Yup.string("Required")
              .email("Please Enter Valid Email")
              .required("Please Enter Email"),
            FirstName: Yup.string()
              .max(
                30,
                "First name is too long, can not be greater than 30 characters"
              )
              .required("First name is required"),
            LastName: Yup.string()
              .max(
                30,
                "Last name is too long, can not be greater than 30 characters"
              )
              .required("Last name is required"),
            Age: Yup.string().required("Age required"),
          })}
          onSubmit={(values, { setSubmitting, resetForm }) => {
            values.Id = new Date().getUTCMilliseconds();
            showTable(true);
            dispatch(userFormAction(values));
            setSubmitting(false);
            resetForm({});
          }}
        >
          <Form>
            <Field name="FirstName" placeholder="First Name" type="text" />
            <ErrorMessage className="error" name="FirstName" />
            <Field name="LastName" placeholder="Last Name" type="text" />
            <ErrorMessage name="LastName" />
            <Field name="Age" placeholder="Age" type="text" />
            <ErrorMessage name="Age" />
            <Field name="Email" placeholder="Email" type="text" />
            <ErrorMessage name="Email" className="error" />
            <div>
              <input type="submit" value="Submit" />
            </div>
          </Form>
        </Formik>
      </div>
      <div className="dv">
        <h1>User Details</h1>
        <table>
          <tbody>
            <tr>
              <td colSpan="8">
                <p>
                  <input
                    type="text"
                    name="search"
                    className="search"
                    onChange={searchinput}
                    placeholder="search"
                  />
                  <input type="submit" value="Search" onClick={search} />
                </p>
              </td>
            </tr>
            <tr>
              <th>Id</th>
              <th colSpan="2" onClick={() => requestSort("FirstName")}>
                FirstName
              </th>
              <th colSpan="2" onClick={() => requestSort("LastName")}>
                LastName
              </th>
              <th colSpan="1" onClick={() => onSortChange()}>
                Age
              </th>
              <th colSpan="2">Email</th>
            </tr>
            {table === true ? (
              info?.map(({ Id, FirstName, LastName, Age, Email }) => {
                return (
                  <tr key={Id}>
                    <td>{Id}</td>
                    <td colSpan={2}>{FirstName}</td>
                    <td colSpan={2}>{LastName}</td>
                    <td>{Age}</td>
                    <td colSpan={2}>{Email}</td>
                  </tr>
                );
              })
            ) : check.length > 0 ? (
              check.map(({ Id, FirstName, LastName, Age, Email }) => {
                return (
                  <tr key={Id}>
                    <td>{Id}</td>
                    <td colSpan={2}>{FirstName}</td>
                    <td colSpan={2}>{LastName}</td>
                    <td>{Age}</td>
                    <td colSpan={2}>{Email}</td>
                  </tr>
                );
              })
            ) : (
              <h6>No Records Found</h6>
            )}
          </tbody>
        </table>
      </div>
    </>
  );
};
export default UserForm;
