import { combineReducers } from 'redux';
import { userFormReducer } from './userFormReducer';

export default combineReducers({
    detail:userFormReducer,
})