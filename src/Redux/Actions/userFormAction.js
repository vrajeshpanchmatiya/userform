import { Data, Searching, Sorting, SortingCharacter } from "../UserType/type";
export const userFormAction = (form) => {
  return (dispatch) => {
    dispatch({ type: Data, payload: form });
  };
};

export const sortingAction = (currentsort) => {
  console.log(currentsort);
  return (dispatch) => {
    dispatch({ type: Sorting, currentsort: currentsort });
  };
};

export const sortingCharacterAction = (key, setConfig) => {
  return (dispatch) => {
    dispatch({ type: SortingCharacter, key: key, setConfig: setConfig });
  };
};

export const searchAction = (find) => {
  return (dispatch) => {
    dispatch({ type: Searching, find });
  };
};
